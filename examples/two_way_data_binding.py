import time

from sync import BaseApp


class BindingDemoApp(BaseApp):
    HTML_BODY = """<h1>{{self.n}}</h1>
                   <input type="checkbox" ng-model="self.b">"""

    def __init__(self):
        super(BindingDemoApp, self).__init__()
        self.n = 0
        self.b = True


if __name__ == "__main__":
    app = BindingDemoApp()
    app.start()
    while 42:
        if app.b:
            app.n += 1
        else:
            print('b is false')
        time.sleep(0.5)
