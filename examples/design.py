from sync import BaseApp, SyncHTML
from sync.design import Bootstrap386Design


class DesignDemoApp(BaseApp):
    TITLE = 'Example Design'
    DESIGN = Bootstrap386Design
    HTML_BODY = SyncHTML('design.shtml').content

    def __init__(self, name):
        super(DesignDemoApp, self).__init__()
        self.name = name
        self.msg = 'This is a simple hero unit, a simple jumbotron-style component for calling ' \
                   'extra attention to featured content or information.'

    def learn_more(self):
        self.msg += ' more knowledge.'


if __name__ == "__main__":
    a = DesignDemoApp('LOL')
    a.start()
