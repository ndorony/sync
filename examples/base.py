from sync import BaseApp, SyncHTML


class BaseDemoApp(BaseApp):
    TITLE = 'Example'
    HTML_BODY = SyncHTML('base.shtml').content

    def __init__(self, name):
        super(BaseDemoApp, self).__init__()
        self.name = name


if __name__ == "__main__":
    a = BaseDemoApp('LOL')
    a.start()
