from sync import BaseApp, SyncList, SyncHTML


class DemoListApp(BaseApp):
    HTML_BODY = SyncHTML('sync_list.shtml').content

    def __init__(self, name):
        super(DemoListApp, self).__init__()
        self.name = name
        self.n = 1
        self.l = SyncList(self, 1, 2, "c", "d")

    def add(self):
        self.l.append(self.name)


if __name__ == "__main__":
    a = DemoListApp('LOL3')
    a.start()
