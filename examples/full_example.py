import os
from os.path import dirname

from sync import BaseApp, SyncList, BaseSubScreen, SyncDict, SyncHTML
from sync.design import MaterialDesign

BASE_PATH = dirname(dirname(__file__))


class App(BaseApp):
    TITLE = 'search strings'
    DESIGN = MaterialDesign
    HTML_BODY = SyncHTML('full_example.shtml').content

    def __init__(self):
        super(App, self).__init__()
        self.new = ''
        self.name = 'LOL'
        self.l = SyncList(self)

    def add(self):
        j = Job(self, self.new)
        self.l.append(j)
        print(self.l._l)
        self.new = ''
        for root, dirnames, filenames in os.walk(BASE_PATH):
            for f in filenames:
                path = os.path.join(root, f)
                j.current = path
                count = open(path, encoding='latin-1').read().count(j.string)
                j.files.append(SyncDict(self, path=path, count=count))
                if count:
                    j.quantity += 1


class Job(BaseSubScreen):
    HTML_BODY = SyncHTML('full_example2.shtml').content

    def __init__(self, app, string):
        super(Job, self).__init__(app)
        self.string = string
        self.quantity = 0
        self.files = SyncList(app)
        self.current = ''


if __name__ == "__main__":
    a = App()
    a.start()
