from sync import BaseApp, SyncHTML


class CallFuncApp(BaseApp):
    HTML_BODY = SyncHTML('call_function.shtml').content

    def __init__(self, name):
        super(CallFuncApp, self).__init__()
        self.name = name

    def twice(self, n):
        self.name *= n


if __name__ == "__main__":
    app = CallFuncApp("LOL")
    app.start()
