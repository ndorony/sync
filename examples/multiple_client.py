from sync import BaseApp, SyncList, SyncHTML


class MultipleClientApp(BaseApp):
    HTML_BODY = SyncHTML('multiple_client.shtml').content

    def __init__(self, name):
        super(MultipleClientApp, self).__init__()
        self.name = name
        self.n = 1
        self.l = SyncList(self, 1, 2, "c", "d")

    def add(self):
        self.l.append(self.name)

    def add_client(self):
        self.open_browser()


if __name__ == "__main__":
    a = MultipleClientApp('LOL3')
    a.start()
