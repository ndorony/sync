from sync import BaseApp, SyncObj


class WithSubObjApp(BaseApp):
    HTML_BODY = """<h1>{{self.sub.sub_value }} </h1>
                   <h1>{{ self.sub.pre.sub.pre.sub.pre.sub.pre.sub.sub_value }} </h1>"""

    def __init__(self, name):
        super(WithSubObjApp, self).__init__()
        self.sub = Sub(self, name)


class Sub(SyncObj):
    def __init__(self, app: WithSubObjApp, val):
        super(Sub, self).__init__(app)
        self.sub_value = val
        self.pre = app


if __name__ == "__main__":
    a = WithSubObjApp('LOL')
    a.start()
