import time

from sync import BaseApp, BaseSubScreen


class WithSubObjApp(BaseApp):
    HTML_BODY = """<h1>{{self.msg }}</h1>
                   <button ng-click="self.open_sub_screen()">open</button>"""

    def __init__(self):
        super(WithSubObjApp, self).__init__()
        self.msg = 'Base screen'
        self._s = SubScreen(self, 'this is small text')

    def open_sub_screen(self):
        self._s.get_focus()


@WithSubObjApp.add_screen
class SubScreen(BaseSubScreen):
    HTML_BODY = '''<h5>{{ self.msg }}</h5>
                  <button ng-click="self.twice(3)">twice</button>
                  <button ng-click="self.main()">main</button>'''

    def __init__(self, app, msg):
        super(SubScreen, self).__init__(app)
        self.msg = msg

    def twice(self, n):
        self.msg *= n

    def main(self):
        self._app.get_focus()


if __name__ == "__main__":
    a = WithSubObjApp()
    a.start()
    for i in range(5):
        a._s.get_focus()
        time.sleep(3)
        a.get_focus()
        time.sleep(3)
