from sync import BaseApp, SyncDict, SyncHTML


class DemoDictApp(BaseApp):
    HTML_BODY = SyncHTML('sync_dict.shtml')

    def __init__(self, name):
        super(DemoDictApp, self).__init__()
        self.d = SyncDict(self, name=name, name_length=len(name))

    def update(self):
        self.d['name_length'] += 1

    def add_new(self):
        self.d['new key'] = 'this is new value'


if __name__ == "__main__":
    a = DemoDictApp('LOL3')
    a.start()
