import asyncio
import os
import subprocess
import threading
import time
from os.path import dirname, join

import tornado
from tornado.web import StaticFileHandler
from tornado.websocket import WebSocketHandler

from sync.core.base_sync_obj import BaseSyncAttr, BaseSyncObj
from sync.core.tornado_server import SocketHandler, get_root_handler, TemplateHandler
from sync.design.base import Design
from . import tornado_server

base_path = dirname(dirname(__file__))
chromium_path = os.path.realpath(join(base_path, 'chromium/chrome.exe'))


class Screen(BaseSyncAttr):
    HTML_BODY = ''

    @classmethod
    def get_ignore_list(cls):
        ignore_list = super(Screen, cls).get_ignore_list()
        ignore_list.add('HTML_BODY')
        ignore_list.add('_get_url')
        return ignore_list

    def _get_url(self, absolute=True):
        base = f'http://127.0.0.1:{self.port}/#' if absolute else ''
        print(f'/{self.__class__.__name__}/{self.obj_id}')
        return f'{base}/{self.__class__.__name__}/{self.obj_id}'

    def get_focus(self):
        self._send({'action': 'change_url', 'url': self._get_url(absolute=False)})

    def __init_subclass__(cls, **kwargs):
        tornado_server.screens[cls.__name__] = cls


class BaseApp(Screen):
    SIZE = '1000,1000'
    DESIGN = Design
    TITLE = ''
    _TEMPLATE = 'base.html'
    _screens = []

    @classmethod
    def title(cls):
        return cls.TITLE if cls.TITLE else cls.__name__

    @classmethod
    def get_ignore_list(cls):
        ignore_list = super(BaseApp, cls).get_ignore_list()
        ignore_list.add('start')
        ignore_list.add('close')
        ignore_list.add('TITLE')
        return ignore_list

    def __init__(self):
        tornado_server.app = self
        self.port = None
        self._ws = set()
        self._thread = None
        self.last_error = None
        super(BaseApp, self).__init__()

    def _set_ws(self, ws: WebSocketHandler):
        self._ws.add(ws)

    def _start_webserver(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        SocketHandler.cls_app = self
        app = tornado.web.Application(
            [(r"/", get_root_handler(self)), (r'/partial/(.+)', TemplateHandler),
             (r"/static/(.+)", StaticFileHandler, dict(path='static')), (r'/ws', SocketHandler)],
            debug=True)

        sockets = tornado.netutil.bind_sockets(0, '127.0.0.1')
        print(sockets)
        server = tornado.httpserver.HTTPServer(app)
        server.add_sockets(sockets)
        for s in sockets:
            self.port = s.getsockname()[:2][1]
        print(f"Application ready and listening @ {self._get_url()}")
        self.open_browser()
        tornado.ioloop.IOLoop.instance().start()

    def start(self):
        self._thread = threading.Thread(target=self._start_webserver, args=())
        self._thread.start()

    def close(self, ws):
        self._ws.remove(ws)
        print(f'GUI Stop, stayed {len(self._ws)}.')

    def _send(self, msg, n=5):
        try:
            print('Sent:', msg)
            for ws in self._ws:
                ws.write_message(msg)
            time.sleep(0.04)
        except BufferError:
            if n:
                time.sleep(0.04)
                self._send(msg, n - 1)

    @classmethod
    def add_screen(cls, screen_cls):
        cls._screens.append(screen_cls)
        return screen_cls

    def open_browser(self, url=None):
        url = url if url else self._get_url()
        subprocess.Popen(f'{chromium_path} --window-size="{self.SIZE}" --app={url}')

    def error_handler(self, exception):
        self.last_error = str(exception)
        print('An exception from the UI was caught ' + exception)


class AppRelatedObj(BaseSyncObj):
    def __init__(self, app: BaseApp):
        self._app = app
        super(AppRelatedObj, self).__init__()

    def _send(self, msg):
        self._app._send(msg)


class BaseSubScreen(Screen, AppRelatedObj):
    pass


class SyncHTML(object):
    def __init__(self, shtml_path):
        if shtml_path and os.path.isfile(shtml_path):
            with open(shtml_path, 'r') as file:
                self.content = file.read()
        else:
            raise Exception(f'Failed to load the SHTML file at: {shtml_path}')