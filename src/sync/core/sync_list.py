from sync.core.base_app import AppRelatedObj
from sync.core.base_app import BaseApp


def extend_func_class(exstend_obj_name, attrs):
    def deco(cls):
        for attr in attrs:
            def func(self, *args, **kwargs):
                return getattr(getattr(self, exstend_obj_name), attr)(*args, **kwargs)

            setattr(cls, attr, func)
        return cls
    return deco


@extend_func_class('_l', ['copy', 'count', 'index', '__len__', '__contains__', '__iter__',
                          '__getitem__'])
class SyncList(AppRelatedObj):
    def __init__(self, app: BaseApp, *args):
        self._l = list(args)
        super(SyncList, self).__init__(app)

    def _start_sync(self, sync_to=None):
        if self._is_sync(sync_to):
            return
        for key, value in enumerate(self._l):
            self._sync(key, value, sync_to)

    def append(self, value):
        self._l.append(value)
        self._sync(len(self._l) - 1, value)

