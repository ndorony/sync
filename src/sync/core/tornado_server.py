import asyncio
import json
import os
import threading

import tornado.ioloop
import tornado.web
import tornado.websocket
from jinja2 import Environment
from jinja2 import FileSystemLoader
from tornado.websocket import WebSocketHandler


objs = {}
screens = {}

app = None


class TemplateHandler(tornado.web.RequestHandler):
    def get(self, screen_name):
        self.write(screens[screen_name].HTML_BODY)
        self.finish()


class SocketHandler(WebSocketHandler):

    def open(self):
        print("WebSocket opened")

    def on_message(self, message):

        message = json.loads(message)
        print(message)
        if message['action'] == 'start':
            app._set_ws(self)
            objs[message['objId']]._start_sync(self)
        elif message['action'] == 'set':
            setattr(objs[message['id']], message['key'], message['value'])
        elif message['action'] == 'call':
            threading.Thread(target=call_func,
                             args=(message['id'], message['key'], message['args'])).start()

    def on_close(self):
        app.close(self)


fs_loader = Environment(
    loader=FileSystemLoader(os.path.join(os.path.dirname(os.path.dirname(__file__)), 'templates')),
    trim_blocks=True)


def get_root_handler(app):
    class RootHandler(tornado.web.RequestHandler):
        def get(self):
            self.write(
                fs_loader.get_template(app._TEMPLATE).render(title=app.title(), body=app.HTML_BODY,
                                                             design=app.DESIGN, screens=screens,
                                                             app=app))
            self.finish()

    return RootHandler


def call_func(obj_id, name, args):
    asyncio.set_event_loop(asyncio.new_event_loop())
    getattr(objs[obj_id], name)(*args.values())
