from sync.core.base_app import AppRelatedObj
from sync.core.base_sync_obj import BaseSyncAttr


class SyncObj(BaseSyncAttr, AppRelatedObj):
    pass


