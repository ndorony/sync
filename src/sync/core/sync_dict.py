from sync.core.base_app import AppRelatedObj
from sync.core.base_app import BaseApp
from sync.core.sync_list import extend_func_class


@extend_func_class('_d', ['__len__', '__contains__', '__iter__',
                          '__getitem__'])
class SyncDict(AppRelatedObj):
    def __init__(self, app: BaseApp, **kwargs):
        self._d = dict(kwargs)
        super(SyncDict, self).__init__(app)

    def _start_sync(self, sync_to=None):
        if self._is_sync(sync_to):
            return
        for key, value in self._d.items():
            self._sync(key, value, sync_to)

    def __setitem__(self, key, value):
        self._d[key] = value
        self._sync(key, value)

