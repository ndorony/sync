import inspect

from . import tornado_server


class BaseSyncObj:

    @classmethod
    def get_ignore_list(cls):
        return {'get_ignore_list', }

    def __init__(self):
        tornado_server.objs.setdefault(self.obj_id, self)
        self._sync_ws = set()

    @property
    def obj_id(self):
        return str(id(self))

    def _is_sync(self, ws):
        if ws in self._sync_ws:
            return True
        self._sync_ws.add(ws)
        return False

    def _sync(self, key, value, ws=None):
        from sync.core.sync_list import SyncList
        from sync.core.sync_dict import SyncDict

        if key in self.get_ignore_list() or not self._is_sync:
            return
        if inspect.ismethod(value):
            self._send({'action': 'set_func', 'key': key, 'id': self.obj_id})
        elif isinstance(value, (str, bool, int)):
            self._send({'action': 'set', 'key': key, 'value': value, 'id': self.obj_id})
        elif isinstance(value, (BaseSyncAttr, SyncDict)):
            self._send({'action': 'set_obj', 'key': key, 'obj_id': value.obj_id, 'id': self.obj_id})
            value._start_sync(ws)
        elif isinstance(value, SyncList):
            self._send(
                {'action': 'set_list', 'key': key, 'obj_id': value.obj_id, 'id': self.obj_id})
            value._start_sync(ws)

    def _send(self, msg):
        raise NotImplementedError()

    def _start_sync(self, sync_to=None):
        raise NotImplementedError()


class BaseSyncAttr(BaseSyncObj):

    def _start_sync(self, sync_to=None):
        if self._is_sync(sync_to):
            return
        for key, value in inspect.getmembers(self):
            if not key.startswith('_'):
                self._sync(key, value, sync_to)

    def __setattr__(self, key: str, value):
        if not key.startswith('_'):
            self._sync(key, value)
        super(BaseSyncObj, self).__setattr__(key, value)
