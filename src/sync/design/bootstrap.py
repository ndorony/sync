from sync.design.base import Design, Css, JavaScript


class BootstrapDesign(Design):
    RESOURCE = [
        Css('https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css'),
        JavaScript('https://code.jquery.com/jquery-3.3.1.slim.min.js'),
        JavaScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js'),
        JavaScript('https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js')]
