from sync.design.base import Design, Css, JavaScript


class Bootstrap386Design(Design):
    RESOURCE = [
        Css('https://cdn.rawgit.com/kristopolous/BOOTSTRA.386/master/v2.3.1/bootstrap/css/bootstrap.css'),
        JavaScript('https://code.jquery.com/jquery-3.3.1.slim.min.js'),
        JavaScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js'),
        JavaScript('https://cdn.rawgit.com/kristopolous/BOOTSTRA.386/master/v2.3.1/bootstrap/js/bootstrap.js')]
