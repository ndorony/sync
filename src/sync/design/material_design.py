from sync.design.base import Design, Css, JavaScript


class MaterialDesign(Design):
    RESOURCE = [
        Css('https://fonts.googleapis.com/icon?family=Material+Icons'),
        Css('https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css'),
        JavaScript('https://code.jquery.com/jquery-3.3.1.slim.min.js'),
        ]