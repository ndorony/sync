from dataclasses import dataclass


@dataclass
class Resource:
    href: str
    head: bool

    def get(self):
        raise NotImplementedError()


@dataclass
class Css(Resource):
    head: bool = True

    def get(self):
        return f'<link type="text/css" rel="stylesheet" href="{self.href}"  media="screen,projection"/>'


@dataclass
class JavaScript(Resource):
    head: bool = False

    def get(self):
        return f'<script type="text/javascript" src="{self.href}"></script>'


class Design:
    RESOURCE = []

